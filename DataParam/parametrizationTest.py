from locust import HttpUser, task, constant, SequentialTaskSet
from readData import CsvRead

class ParamScript(SequentialTaskSet):

    @task
    def add_user(self):
        test_data = CsvRead("DataParam\\users-data.csv").read()
        print(test_data)

        data = {
            "name": test_data['name'],
            "age": test_data['age'],
            "gender": test_data['gender'],
            "email": test_data['gender']
        }

        name = "Client " + test_data['name']

        with self.client.post("/post", catch_response=True, name=name, data=data) as response:
            if response.status_code == 200 and test_data['name'] in response.text:
                response.success()
            else:
                response.failure("Failure processing the client")

class MyLoadTest(HttpUser):
    host = "https://httpbin.org"
    wait_time = constant(1)
    tasks = [ParamScript]