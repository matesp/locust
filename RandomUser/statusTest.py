from locust import HttpUser, task, constant, SequentialTaskSet

class MyReqRes(SequentialTaskSet):
    wait_time = constant(1)

    @task
    def get_random_user(self):
        res = self.client.get("/")
        print("GET method status is ", res.status_code)

class MySeqTest(HttpUser):
    wait_time = constant(1)
    host = "https://randomuser.me/api"

    tasks = [MyReqRes]
